# Installing Battle of four Armies
Updated 05.05.2019

[< Setup][0]

For ease of navigation, the installation is broken up into sections and can be jumped to by clicking any of the links below.

1. [Required Software](#required-software)
2. [Installing BFA](#installing-bfa)

## Required Software

#### 1. **[Steam](http://steampowered.com)**
#### 2. Dota 2 Workshop Tools
 - From within steam, right click on `Dota 2` and select `Properties` at the bottom
   ![Dota 2 Tools Install Pt. 1](../.images/BFA_Install_D2Tools_1.png)
 - Go to DLC tab of the window that pops up
 - Check the `INSTALL` box next to `Dota 2 Workshop Tools DLC`
   ![Dota 2 Tools Install Pt. 2](../.images/BFA_Install_D2Tools_2.png)
 - It will download once you close that window (This can take a bit)
#### 3. [SourceTree](https://www.sourcetreeapp.com/) or any other git-client
#### 4. [NodeJS](https://nodejs.org/)
#### 5. [Visual Studio Code](https://code.visualstudio.com/) or any other text editor/IDE that supports `.editorconfig`
#### 6. [TypeScript](https://www.typescriptlang.org/)
#### 7. [TypeScriptForLua](https://github.com/TypeScriptToLua) only if you are working on server files

## Installing BFA

[0]: README.md