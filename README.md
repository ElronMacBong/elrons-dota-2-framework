# Elron's Dota 2 Framework [![All Contributors](https://img.shields.io/badge/all_contributors-1-orange.svg?style=flat-square)](#contributors)
<!--![Build Status](https://img.shields.io/circleci/project/all-contributors/all-contributors/master.svg)-->
![Pipeline Status](https://gitlab.com/legendary-experience/elrons-dota-2-framework/badges/development/pipeline.svg)
![Coverage Report](https://gitlab.com/legendary-experience/elrons-dota-2-framework/badges/development/coverage.svg)
<!--![Crowdin](https://d322cqt584bo4o.cloudfront.net/all-contributors/localized.svg)-->

<!--
<table>
    <caption>Read this documentation in the following languages</caption>
    <tbody>
        <tr>
            <td><a href="https://allcontributors.org/docs/ko/overview">한국어</a></td>
            <td><a href="https://allcontributors.org/docs/zh-CN/overview">中文</a></td>
            <td><a href="https://allcontributors.org/docs/id/overview">Bahasa Indonesia</a></td>
            <td><a href="https://allcontributors.org/docs/de/overview">Deutsch</a></td>
            <td><a href="https://allcontributors.org/docs/pl/overview">Polskie</a></td>
        </tr>
        <tr>
            <td><a href="https://allcontributors.org/docs/en/overview">English</a></td>
            <td><a href="https://allcontributors.org/docs/pt-BR/overview">Português</a></td>
            <td><a href="https://allcontributors.org/docs/es-ES/overview">Español</a></td>
            <td><a href="https://allcontributors.org/docs/fr/overview">Français</a></td>
        </tr>
    </tbody>
</table>
-->

A really great Dota 2 framework built by [really great people](#contributors).

# Getting Started

# Contributing

### Suggestions?
Do not create or comment on tickets in this repository for game suggestions. Use [the suggestion repo](https://gitlab.com/legendary-experience/bfa_suggestions/issues).

## Contributors
Thanks goes to these wonderful people ([emoji key](https://github.com/kentcdodds/all-contributors#emoji-key)):

<!-- ALL-CONTRIBUTORS-LIST:START - Do not remove or modify this section -->
<!-- prettier-ignore -->
<table><tr><td align="center"><a href="https://gitlab.com/ElronMacBong"><img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/1517780/avatar.png" width="100px;" alt="Elron MacBong"/><br /><sub><b>Elron MacBong</b></sub></a><br /><a href="#projectManagement-ElronMacBong" title="Project Management">📆</a> <a href="https://gitlab.com/Flam3s/BFA/commits/master" title="Code">💻</a> <a href="#ideas-ElronMacBong" title="Ideas, Planning, & Feedback">🤔</a> <a href="#translation-ElronMacBong" title="Translation">🌍</a> <a href="https://gitlab.com/Flam3s/BFA/commits/master" title="Tests">⚠️</a> <a href="#gameDesign-ElronMacBong" title="Game design">🤔</a> <a href="#balance-ElronMacBong" title="Balance">📈</a></td></tr></table>

<!-- ALL-CONTRIBUTORS-LIST:END -->

This project follows the [all-contributors](https://github.com/kentcdodds/all-contributors) specification.
Contributions of any kind welcome!

## License
[GNU GPLv3](LICENSE)