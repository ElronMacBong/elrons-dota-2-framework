declare interface Timers {
    CreateTimer(delay: number, callback: (this: void) => void | number): void;
    CreateTimer<T>(delay: number, callback: (this: void, context: T) => void | number, context: T): void;
    RemoveTimer(name: string, callback: (this: void) => void | string): void;
    RemoveTimers(killAll: boolean, callback: (this: void) => void | string): void;
}

declare var Timers: Timers;